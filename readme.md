<div align="center">
    <h1>
        <img src="repo_assets/tux.png" width="96"><br>
        thom's config. files
    </h1>
</div>

![screenshot](repo_assets/desktop_screenshot.png)

This repo contains my configuration files for unix-like systems. See the [example programs](arch_example_programs.txt) for a list of packages which could be installed to get everything working.

Overview
--------

Systems are intended to be as minimal and [free](https://www.gnu.org/philosophy/free-sw.en.html) as is practical.

- The system can be Wayland-based and uses [river](https://github.com/riverwm/river) as a compositor and for window management
- The rest of the desktop environment configurations consist of [i3status-rust](https://github.com/greshake/i3status-rust) with an '[adapter](https://github.com/MaxVerevkin/i3bar-river)' for river and [fuzzel](https://codeberg.org/dnkl/fuzzel) to select applications and launch custom shortcut scripts
- Basic tasks like text editing and file management are handled using minimal - usually terminal-based - applications ([helix](https://github.com/helix-editor/helix) and [yazi](https://github.com/sxyazi/yazi) respectively)
- Other software which can be used is listed in [arch_example_programs.txt](arch_example_programs.txt)
