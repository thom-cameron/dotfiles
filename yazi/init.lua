-- ui tweaks
require("full-border"):setup {
	type = ui.Border.ROUNDED,
}
require("dynamic-columns"):setup()
require("detailed-path"):setup()
require("full-prompt"):setup()
