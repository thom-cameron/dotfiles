$env.config = {
    show_banner: false

    completions: {
        case_sensitive: false
        quick: true   
        partial: true   
        algorithm: "prefix"
        sort: "smart"
        external: {
            enable: true
            max_results: 100
            completer: null
        }
        use_ls_colors: true
    }

    cursor_shape: {
        emacs: line
        vi_insert: block
        vi_normal: block
    }

    edit_mode: emacs
    render_right_prompt_on_last_line: false

    hooks: {
        pre_prompt: [{ print "" }]
        pre_execution: [{ null }]
        env_change: {
            PWD: [{|before, after| null }]
        }
    }

    menus: [
        {
            name: history_menu
            only_buffer_difference: true
            marker: "? "
            type: {
                layout: list
                page_size: 10
            }
            style: {
                text: white
                selected_text: green_reverse
                description_text: yellow
            }
        }
    ]

    keybindings: [
        {
            name: delete_word_backward
            modifier: control
            keycode: char_h
            mode: [emacs, vi_insert]
            event: { edit: backspaceword }
        }
        {
            name: newline
            modifier: control
            keycode: enter
            mode: [emacs, vi_normal, vi_insert]
            event: { edit: insertnewline }
        }
        {
            name: open_command_editor
            modifier: control
            keycode: char_e
            mode: [emacs, vi_normal, vi_insert]
            event: { send: openeditor }
        }
        {
            name: quit_shell
            modifier: control
            keycode: char_q
            mode: [emacs, vi_normal, vi_insert]
            event: { send: ctrld }
        }
        {
            name: fzf_go
            modifier: control
            keycode: char_g
            mode: [emacs, vi_insert, vi_normal]
            event:[
                { 
                    edit: InsertString,
                    value: 'cd (fzf --walker "dir,hidden" --preview "eza --tree --level 1 --colour=always {}" --height 40%)'
                }
                { send: Enter }
            ]
        }
        {
            name: open_yazi
            modifier: control
            keycode: char_f
            mode: [emacs, vi_insert, vi_normal]
            event:[
                { 
                    edit: InsertString,
                    value: 'yazi'
                }
                { send: Enter }
            ]
        }
    ]
}

# zoxide
source ~/.zoxide.nu

# aliases
alias cd  = z
alias cat = bat
alias ll = ls -l
alias la = la -a
alias tree = eza --tree
